#!/bin/bash
set -xe
composer update
./vendor/bin/phinx migrate -c ./app/config/phinx.php
php-fpm
