<?php
return
    [
        'paths' => [
            'migrations' => '%%PHINX_CONFIG_DIR%%/../migrations',
        ],
        'environments' =>
            [
                'default_database' => 'production',
                'default_migration_table' => 'phinxlog',
                'production' =>
                    [
                        'adapter' => 'sqlite',
                        'name' => $_ENV['DB_NAME'],
                        'suffix' => '',
                    ],
            ],
    ];