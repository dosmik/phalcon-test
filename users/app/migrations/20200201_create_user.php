<?php

use Phinx\Migration\AbstractMigration;
use Phalcon\Security;



class CreateUser extends AbstractMigration
{

    public function change()
    {
        $user = $this->table('user');
        $user->addColumn('login', 'string', ['limit' => 255])
              ->addColumn('password', 'string', ['limit' => 255])
              ->create();
        $security = new Security();
        $admin = [
            'login' => 'admin',
            'password' => $security->hash('admin')
        ];
        $user->insert([$admin])->save();
    }
}
