<?php declare(strict_types=1);

namespace app\controllers;

use app\forms\Auth as FormAuth;
use Datto\JsonRpc\Client;
use Phalcon\Http\Message\Stream\Temp as StreamTemp;
use GuzzleHttp\Psr7\Response as ClientResponse;
use Phalcon\Http\RequestInterface;


class IndexController extends \Phalcon\Mvc\Controller
{
    private function getForm(): FormAuth
    {
        return $this->getDI()->get(FormAuth::class);
    }

    public function indexAction()
    {
        $this->view->form = $this->getForm();
        return $this->view->render('index', ['index']);
    }

    public function authAction()
    {
        $form = $this->getForm();
        $this->view->form = $form;

        if ($this->request->isPost()) {
            if ($this->security->checkToken()) {
                if (!$form->isValid($this->request->getPost())) {
                    foreach ($form->getMessages() as $body) {
                        $this->flash->error($body->getMessage());
                    }
                    return $this->view->render('index', ['index']);
                }

                $body = $this->prepareRequestBody($this->request);
                $response = $this->sendRequest($body);

                if ($response->getStatusCode() !== 200) {
                    $this->flash->error('Error API Request');
                } else {
                    $this->view->response = $response->getBody()->getContents();
                }
                $this->view->request = $body;

            } else {
                $this->flash->error('CSRF Error');
            }
        }


        return $this->view->render('index', ['index']);
    }

    private function prepareRequestBody(RequestInterface $request): string
    {
        $client = new Client();
        $client->query(
            1,
            'auth.index',
            [
                'login' => $this->request->getPost('login'),
                'password' => $this->request->getPost('password')
            ]
        );

        return $client->encode();
    }

    private function sendRequest(string $body): ClientResponse
    {
        $message = new StreamTemp('a+');
        $message->write($body);
        /** @var  \Phalcon\Http\Message\Request $request */
        $request = $this->getDI()->get('api.users')->withBody($message);
        /** @var \GuzzleHttp\Client $client */
        $client = $this->getDI()->get('http.client');
        $response = $client->send($request);
        return $response;
    }
}
