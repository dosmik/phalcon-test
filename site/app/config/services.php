<?php
declare(strict_types=1);

use Phalcon\Mvc\View\Simple as View;
use Phalcon\Session\Manager;
use Phalcon\Session\Adapter\Stream;
use Phalcon\Url as UrlResolver;
use Phalcon\Http\Message\Request;

// get available provider Curl or Stream
//$provider = Request::getProvider();

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * Sets the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    return $view;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});

$di->setShared(
    'session',
    function () {
        $session = new Manager();
        $files = new Stream(
            [
                'savePath' => '/tmp',
            ]
        );
        $session->setAdapter($files);

        $session->start();

        return $session;
    }
);

$di->setShared('api.users',
    function () {
        $request = new \Phalcon\Http\Message\Request(
            'POST',
            'http://users_nginx/',
            'php://memory',
            [
                'Content-Type'  => 'application/json',
            ]
        );

        return $request;
    }
);

$di->setShared('http.client',
    function () {
        $client = new \GuzzleHttp\Client(['timeout'  => 1.0, 'allow_redirects' => false]);
        return $client;
    }
);
