run-dev all:

run-dev:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build

clean:
	docker-compose rm -svf
	rm -rf ./site/vendor ./site/composer.lock ./users/vendor ./users/composer.lock
